## PRTG Device Template for Cisco Wireless LAN Controller devices

This project is a custom device template that cna be used to monitor a Cisco Wireless LAN Controller in PRTG using the auto-discovery for sensor creation.

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/Cisco_WLC/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [How can I monitor a Cisco WLC deployment with PRTG?](https://kb.paessler.com/en/topic/75561)